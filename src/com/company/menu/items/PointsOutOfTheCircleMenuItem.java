package com.company.menu.items;

import com.company.Circle;
import com.company.Point;
import com.company.PointList;
import com.company.menu.MenuItem;

public class PointsOutOfTheCircleMenuItem implements MenuItem {
    PointList pointList;
    Circle circle;

    public PointsOutOfTheCircleMenuItem(PointList pointList, Circle circle) {
        this.pointList = pointList;
        this.circle = circle;
    }

    @Override
    public String getName() {
        return "Points out of the circle: ";
    }

    @Override
    public void execute() {
        System.out.println("Points out of the circle: ");
        for (int i = 0; i < pointList.getNumberOfPoints(); i++) {
            Point p = pointList.getPoint(i);
            if (!circle.containsPoint(p)) {
                System.out.println(p);
            }
        }
    }
}
