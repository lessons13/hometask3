package com.company.menu.items;

import com.company.Point;
import com.company.PointList;
import com.company.menu.MenuItem;

import java.util.Scanner;

public class AddPointMenuItem implements MenuItem {
    PointList pointList = new PointList();
    Scanner scanner;

    public AddPointMenuItem(Scanner scanner, PointList pointList) {
        this.scanner = scanner;
        this.pointList = pointList;
    }

    @Override
    public String getName() {
        return "Add point";
    }

    @Override
    public void execute() {
        System.out.print("|Enter point coordinates\n |x: ");
        if (!scanner.hasNextDouble()) {
            System.out.println("Incorrect input");
            scanner.nextLine();
            return;
        }
        double x = scanner.nextDouble();
        System.out.print(" |y: ");
        if (!scanner.hasNextDouble()) {
            System.out.println("Incorrect input");
            scanner.nextLine();
            return;
        }
        double y = scanner.nextDouble();
        pointList.addPoint(new Point(x, y));
    }
}
