package com.company.menu;

public interface MenuItem {
    String getName();
    void execute();

    default boolean isFinal() {
        return false;
    }

}
